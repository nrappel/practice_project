library(shiny)

# Define UI for application that draws a histogram
shinyUI(fluidPage(
  
  # Define own style-sheet: 
  tags$head(
    tags$link(rel = "stylesheet", type = "text/css", href = "simulation_app_style.css")
  ),
  
  # Application title
  titlePanel("Simulation-Tool (Turbines)"),
  
  tabsetPanel(
    tabPanel("Basic widgets",
             fluidRow(
               
               column(3,
                      dateRangeInput("dates", label = h3("Date range"))),
               
               column(3,
                      fileInput("file", label = h3("File input"))),
               
               column(3, 
                      h3("Help text"),
                      helpText("Note: help text isn't a true widget,", 
                               "but it provides an easy way to add text to",
                               "accompany other widgets.")),
               
               column(3, 
                      numericInput("num", 
                                   label = h3("Numeric input"), 
                                   value = 1))   
             ),
             
             fluidRow(
               
               column(3,
                      radioButtons("radio", label = h3("Radio buttons"),
                                   choices = list("Choice 1" = 1, "Choice 2" = 2,
                                                  "Choice 3" = 3),selected = 1)),
               
               column(3,
                      selectInput("select", label = h3("Select box"), 
                                  choices = list("Choice 1" = 1, "Choice 2" = 2,
                                                 "Choice 3" = 3), selected = 1)),
               
               column(3, 
                      sliderInput("slider1", label = h3("Sliders"),
                                  min = 0, max = 100, value = 50),
                      sliderInput("slider2", "",
                                  min = 0, max = 100, value = c(25, 75))
               ),
               
               column(3, 
                      textInput("text", label = h3("Text input"), 
                                value = "Enter text..."))   
             )
    ),
    
    
    tabPanel(
      "Shiny-Basics",
      sidebarPanel(
        h5("Table-Of-Content"),
        a("Paragraph", href = "#paragraph"),
        br(),
        a("Header", href = "#header"),
        br(),
        a("Code-Blocks", href = "#code_blocks"),
        br(),
        a("Images", href = "#images")
      ),
      mainPanel(
        width = 8,
        
        div(id = "paragraph",
          h1("Paragraph"),
          em("italic-text"),
          HTML("<b>bold-text</b>"),
          p("Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.")
        ),
        
        div(id = "header",
          h1("h1-header"),
          h2("h2-header"),
          h3("h3-header"),
          h4("h4-header")
        ),
        
        div(id = "code_blocks",
          h1("Code-Blocks"),
          p("This is a example for a simple code-block: "),
          code("<body><p>test-code-content</p></body>")
        ),
        
        div(id = "images",
          h1("Images"),  
          img(src = "images/html5.jpg", width = "50%")
        )
      )
    ),
    
    tabPanel(
      "Turbine-Plot",
      
      # Sidebar with a slider input for the number of bins
      sidebarLayout(
        sidebarPanel(
          sliderInput("turbine_select", "Number of turbines:",min = 1,max = 50,value = 30),
          
          # Dropdown-Menu
          div(
            style = "display:inline-block; width: 100%;",
            selectInput(
              "plane_select",
              width = "100%",
              "Plane:",
              choices = c('Airbus-A380' = '1',
                          'Boin-747' = '2')
            )
          ),
          
          radioButtons(
            "dist", "Distribution type:",
            c(
              "Normal" = "norm",
              "Uniform" = "unif",
              "Log-normal" = "lnorm",
              "Exponential" = "exp"
            )
          )
        ),
        
        # Show a plot of the generated distribution
        mainPanel(plotOutput("simulationPlot"))
      )
    ),
    
    tabPanel(
      "Data-Upload", 
      sidebarLayout(
        sidebarPanel(
          fileInput('file1', 'Choose file to upload',
                    accept = c(
                      'text/csv',
                      'text/comma-separated-values',
                      'text/tab-separated-values',
                      'text/plain',
                      '.csv',
                      '.tsv'
                    )
          ),
          tags$hr(),
          checkboxInput('header', 'Header', TRUE),
          radioButtons('sep', 'Separator',
                       c(Comma=',',
                         Semicolon=';',
                         Tab='\t'),
                       ','),
          radioButtons(
            'quote', 
            'Quote',
            c(None='', 'Double Quote'='"', 'Single Quote'="'"), 
            '"'
          )
        ),
        
        mainPanel(
          dataTableOutput('csvContents')
        )
      )
    )
    
    
  ))
)