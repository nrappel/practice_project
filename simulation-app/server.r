library(shiny)

# Test-Git-Commentary!

# By default, the file size limit is 5MB. It can be changed by
# setting this option. Here we'll raise limit to 9MB.
options(shiny.maxRequestSize = 9*1024^2)

shinyServer(function(input, output) {
  
  #
  # Simulation-Tab
  #
  output$simulationPlot <- renderPlot({
    tightened_screw <- faithful[, 2] 
    
    turbines <- seq(
      min(tightened_screw), 
      max(tightened_screw), 
      length.out = input$turbine_select + 1
    )
    
    hist(
      tightened_screw, 
      xlab='# of tightened screws', 
      ylab='# of system errors', 
      main='screws vs. system errors', 
      breaks = turbines, 
      col = 'darkblue', 
      border = 'white')
  })
  
  
  
  #
  # CSV-Upload-Tab
  #
  output$csvContents <- renderDataTable({
    # input$file1 will be NULL initially. After the user selects
    # and uploads a file, it will be a data frame with 'name',
    # 'size', 'type', and 'datapath' columns. The 'datapath'
    # column will contain the local filenames where the data can
    # be found.
    
    inFile <- input$file1
    
    if (is.null(inFile))
      return(NULL)
    
    read.csv(
      inFile$datapath, 
      header = input$header,
      sep = input$sep, 
      quote = input$quote
    )
  })
  
  
})